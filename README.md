# election_results

This repository stores election results, data, and code for reports around SF DSA elections. This allows for transparency and reproducibility around elections that have been held in the chapter, a requirement of the bylaws. 

Currently, this repository stores information about the following elections:

* The June 19th, 2017 Steering Committee and National Delegation elections. This is found in the `election_6_19_17` subfolder.

