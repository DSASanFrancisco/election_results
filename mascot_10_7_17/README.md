# November 7th DSA SF Mascot Election

This folder relates to the November 7th DSA SF Mascot Election.

# Contents

This folder contains two components:

* The raw data from the election as obtained by Jesse from the election database, contained in the `raw_data` folder.
* An R Project to:
  * Parse the raw data into various forms, the output of which is found in the `data` folder;
  * Validate the parsed data, 
  * And generate some animated gifs that visualize the elections
    
## Browsing

If you want an easy way of looking up your ballot with the names of your choices, click into the `data` folder and open up the  `mascot_votes.csv` file. Github will render them appropriately and allow you to type in your ballot id that you received when voting. For convenience, you can access them here:

## Usage

* Install [RStudio](http://rstudio.com) and [R](https://www.r-project.org). You'll probably also need some standard tools to compile C++, etc. On OSX, a working install of homebrew should likely ensure this.
* Open up the `Rproj` file in this folder in RStudio. This will bootstrap `packrat` and install all the package dependencies.  
* Run the `Clean And Rebuild` item in the `Build` pane. This is using the `Makefile` in this directory, so you can alternatively run `make clean && make all`.
* This will output the processed data and animated gifs in the `data` and `reports` folders. 
* There are tests in the `tests` folder that can be ran using `make tests` or manually using the `testthat` package. These guarantee that the number of candidates, ballots, etc. are correct.